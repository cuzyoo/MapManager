package ch.levingo1.mapmanager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import ch.levingo1.mapmanager.command.CmdAllow;
import ch.levingo1.mapmanager.command.CmdBuildspawn;
import ch.levingo1.mapmanager.command.CmdCreate;
import ch.levingo1.mapmanager.command.CmdDisallow;
import ch.levingo1.mapmanager.command.CmdInfo;
import ch.levingo1.mapmanager.command.CmdList;
import ch.levingo1.mapmanager.command.CmdName;
import ch.levingo1.mapmanager.command.CmdRemove;
import ch.levingo1.mapmanager.command.CmdTp;
import ch.levingo1.mapmanager.config.SimpleConfigManager;
import ch.levingo1.mapmanager.util.Cuboid;

public class MapManager extends JavaPlugin {

	public static MapManager plugin;
	public final Logger logger = Logger.getLogger("Minecraft");
	List<String> team = new ArrayList<>();
	SimpleConfigManager confManager = null;

	@Override
	public void onDisable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + " Has Been Disabled!");
	}

	@Override
	public void onEnable() {
		confManager = new SimpleConfigManager(this);
		PluginManager pm = getServer().getPluginManager();

		pm.registerEvents(new Regular(this), this);
		this.getConfig().options().copyDefaults(true);
		saveConfig();
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (sender instanceof Player) {
			final Player p = (Player) sender;
			if (commandLabel.equalsIgnoreCase("map")) {
				if (args.length == 0) {
					showHelp(p);
				} else {
					switch (args[0].toLowerCase()) {
					case "create":
						new CmdCreate(this, p, args);
						break;
					case "tp":
						new CmdTp(this, p, args);
						break;
					case "list":
						new CmdList(this, p, args);
						break;
					case "allow":
						new CmdAllow(this, p, args);
						break;
					case "disallow":
						new CmdDisallow(this, p, args);
						break;
					case "info":
						new CmdInfo(this, p, args);
						break;
					case "remove":
						new CmdRemove(this, p, args);
						break;
					case "buildspawn":
						new CmdBuildspawn(this, p, args);
						break;
					case "name":
						new CmdName(this, p, args);
						break;
					default:
						showHelp(p);
						break;
					}

					saveConfig();
				}
			}
		}
		return false;
	}

	public boolean canBuild(Player p, String world) {
		try {
			if (getConfig().getString("worlds." + world + ".owner").equalsIgnoreCase(p.getName())) {
				return true;
			}

			if (getConfig().getList("worlds." + world + ".builders").contains(p.getName())) {
				return true;
			}

			return false;
		} catch (Exception ex) {
			return false;
		}

	}

	public boolean isOwner(Player p, String world) {
		if (getConfig().getString("worlds." + world + ".owner").equalsIgnoreCase(p.getName())) {
			return true;
		}

		return false;
	}

	public void showHelp(Player p) {
		p.sendMessage(" §6Commands:");
		p.sendMessage("");
		p.sendMessage(" §7/map create");
		p.sendMessage(" §7/map list < all / mine / online >");
		p.sendMessage(" §7/map tp <ID>");
		p.sendMessage(" §7/map allow <Playername>");
		p.sendMessage(" §7/map disallow <Playername>");
		p.sendMessage(" §7/map info <Map>");
		p.sendMessage(" §7/map remove");
		p.sendMessage(" §7/map buildSpawn <Map>");
		p.sendMessage(" §7/map name <Name> (spaces allowed!)");
	}

	@Override
	public FileConfiguration getConfig() {
		return super.getConfig();
	}

	public SimpleConfigManager getConfManager() {
		return confManager;
	}

	public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
		return new Generator(this);
	}
}
