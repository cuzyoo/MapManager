package ch.levingo1.mapmanager;

import java.util.Random;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;

public class Generator extends ChunkGenerator {
	MapManager plugin;

	public Generator(MapManager plugin) {
		this.plugin = plugin;
	}

	public byte[][] generateBlockSections(World world, Random random, int chunkX, int chunkZ, ChunkGenerator.BiomeGrid biomeGrid) {
		byte[][] result = new byte[world.getMaxHeight() / 16][];
		int x = 0;
		int y = 64;
		int z = 0;
		if ((x >= 0) && (z >= 0)) {
			if ((x / 16 == chunkX) && (z / 16 == chunkZ))
				setBlock(result, x % 16, y, z % 16, (byte) 7);
		} else if ((x >= 0) && (z < 0)) {
			if ((x / 16 == chunkX) && (z / 16 == chunkZ + 1))
				setBlock(result, x % 16, y, 16 + z % 16, (byte) 7);
		} else if ((x < 0) && (z >= 0)) {
			if ((x / 16 == chunkX + 1) && (z / 16 == chunkZ)) {
				setBlock(result, 16 + x % 16, y, z % 16, (byte) 7);
			}
		} else if ((x / 16 == chunkX + 1) && (z / 16 == chunkZ + 1)) {
			setBlock(result, 16 + x % 16, y, 16 + z % 16, (byte) 7);
		}

		return result;
	}

	public Location getFixedSpawnLocation(World world, Random random) {
		return new Location(world, 0, 65, 0);
	}

	private void setBlock(byte[][] result, int x, int y, int z, byte blkid) {
		if (result[(y >> 4)] == null) {
			result[(y >> 4)] = new byte[4096];
		}
		result[(y >> 4)][((y & 0xF) << 8 | z << 4 | x)] = blkid;
	}
}