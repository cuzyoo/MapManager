package ch.levingo1.mapmanager;

import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class Regular implements Listener {

	MapManager main;

	public Regular(MapManager main) {
		this.main = main;
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		if(!main.canBuild(e.getPlayer(), e.getBlock().getWorld().getName())){
			e.setCancelled(true);
			e.getPlayer().sendMessage("§4You're not allowed to build here!");
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		if(!main.canBuild(e.getPlayer(), e.getBlock().getWorld().getName())){
			e.setCancelled(true);
			e.getPlayer().sendMessage("§4You're not allowed to build here!");
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		if(!main.canBuild(e.getPlayer(), e.getPlayer().getWorld().getName())){
			e.setCancelled(true);
			e.getPlayer().sendMessage("§4You're not allowed to modify stuff here!");
		}
	}
}