package ch.levingo1.mapmanager.command;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;

import ch.levingo1.mapmanager.MapManager;

public class CmdInfo implements Cmd {
	private MapManager mm;

	public CmdInfo(MapManager mm, Player p, String[] args) {
		super();
		this.mm = mm;
		execute(p, args);
	}

	@Override
	public void execute(Player p, String[] args) {
		String world = "";

		if (args.length != 2) {
			world = p.getWorld().getName();
		} else {
			world = args[1];

		}

		try {
			p.sendMessage("§e--- Info about the world with ID '§b" + world + "§e' ---");
			p.sendMessage("§7Name: " + "§6" + mm.getConfig().getString("worlds." + world + ".name"));
			p.sendMessage("§7Created: " + "§6" + mm.getConfig().getString("worlds." + world + ".created"));
			p.sendMessage("§7Owner: " + "§6" + mm.getConfig().getString("worlds." + world + ".owner"));
			p.sendMessage("§7Helpers:");
			for (Object s : mm.getConfig().getList("worlds." + world + ".builders")) {
				p.sendMessage("§6- " + s.toString());
			}
			p.sendMessage("");
		} catch (Exception ex) {
			p.sendMessage("§7Hmm.. I can't find any info about this world... :(");
		}

	}

}
