package ch.levingo1.mapmanager.command;

import java.util.ArrayList;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import ch.levingo1.mapmanager.MapManager;

public class CmdDisallow implements Cmd{
	private MapManager mm;

	public CmdDisallow(MapManager mm, Player p, String[] args) {
		super();
		this.mm = mm;
		execute(p, args);
	}

	@Override
	public void execute(Player p, String[] args) {
		if (args.length != 2) {
			mm.showHelp(p);
		} else {
			if (mm.getConfig().getString("worlds." + p.getWorld().getName() + ".owner").equalsIgnoreCase(p.getName())) {
				ArrayList<String> builders;
				try {
					builders = (ArrayList<String>) mm.getConfig().getList("worlds." + p.getWorld().getName() + ".builders");
					if (builders.contains(args[1])) {
						builders.remove(args[1]);
						p.sendMessage("§4Removed §6" + args[1] + "§4 from this map!");
					} else {
						p.sendMessage("§6" + args[1] + " §4doesn't help here...");
					}

				} catch (Exception ex) {
					builders = new ArrayList<>();
				}

				mm.getConfig().set("worlds." + p.getWorld().getName() + ".builders", builders);
				mm.saveConfig();

			} else {
				p.sendMessage("§4You're not the owner!");
			}
		}
	}
	
	
}
