package ch.levingo1.mapmanager.command;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import ch.levingo1.mapmanager.MapManager;

public class CmdList implements Cmd {
	private MapManager mm;

	public CmdList(MapManager mm, Player p, String[] args) {
		super();
		this.mm = mm;
		execute(p, args);
	}

	@Override
	public void execute(Player p, String[] args) {

		if (args.length == 2) {
			p.sendMessage(" §f§lWORLDS");
			p.sendMessage(" §f§m------------------");
			p.sendMessage("");
			p.sendMessage(" §7▉ §fCan't build");
			p.sendMessage(" §a▉ §fCan build");
			p.sendMessage(" §d▉ §fYou're the owner");
			p.sendMessage("");
			ArrayList<String> allWorlds = null;
			try {
				allWorlds = (ArrayList<String>) mm.getConfig().getList("worldlist");
			} catch (Exception ex) {
				allWorlds = new ArrayList<>();
			}

			ArrayList<String> finalWorlds = new ArrayList<>();

			switch (args[1]) {
			case "all":
				finalWorlds = allWorlds;
				break;
			case "mine":
				for (String s : allWorlds) {
					if (mm.isOwner(p, s) || mm.canBuild(p, s)) {
						finalWorlds.add(s);
					}
				}
				break;
			case "online":
				for (String s : allWorlds) {
					if (!Bukkit.getWorld(s).getPlayers().isEmpty()) {
						finalWorlds.add(s);
					}
				}
				break;
			default:
				break;
			}

			try {

				for (String s : finalWorlds) {
					if (mm.canBuild(p, s)) {
						if (mm.isOwner(p, s)) {
							p.sendMessage(" " + s + ":§d " + mm.getConfig().getString("worlds." + s + ".name"));
						} else {
							p.sendMessage(" " + s + ":§a " + mm.getConfig().getString("worlds." + s + ".name"));
						}
					} else {
						p.sendMessage(" " + s + ": §7" + mm.getConfig().getString("worlds." + s + ".name"));
					}

				}
			} catch (NullPointerException ex) {
				p.sendMessage(" §7Argh! There are no maps!");
			}

			p.sendMessage("");
			p.sendMessage(" §b§k|§r§3 You can teleport yourself to a map using /map tp <ID> §b§k|");

		} else {
			p.sendMessage(" §4/map list < §6all§4 /§6 mine§4 / §6online §4>");
		}

	}

}
