package ch.levingo1.mapmanager.command;

import org.bukkit.entity.Player;

public interface Cmd {
	public void execute(Player p, String[] args);
}
