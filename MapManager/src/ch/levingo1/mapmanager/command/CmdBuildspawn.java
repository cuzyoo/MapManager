package ch.levingo1.mapmanager.command;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;

import ch.levingo1.mapmanager.MapManager;
import ch.levingo1.mapmanager.util.Cuboid;

public class CmdBuildspawn implements Cmd {
	private MapManager mm;

	public CmdBuildspawn(MapManager mm, Player p, String[] args) {
		super();
		this.mm = mm;
		execute(p, args);
	}

	@Override
	public void execute(Player p, String[] args) {
		String world = "";
		if(args.length == 2){
			world = args[1];
		}else{
			world = p.getWorld().getName();
		}
		
		if (mm.getConfig().getString("worlds." + world + ".owner").equalsIgnoreCase(p.getName())) {
			
			World w = mm.getServer().getWorld(world);
			Location l1 = w.getSpawnLocation();
			l1.getBlock().setType(Material.BEDROCK);
			p.sendMessage("§aSpawn has been rebuilt!");

		} else {
			p.sendMessage("§4You're not the owner!");
		}

	}

}
