package ch.levingo1.mapmanager.command;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import ch.levingo1.mapmanager.MapManager;

public class CmdTp implements Cmd {
	private MapManager mm;

	public CmdTp(MapManager mm, Player p, String[] args) {
		super();
		this.mm = mm;
		execute(p, args);
	}

	@Override
	public void execute(Player p, String[] args) {
		if (args.length != 2) {
			mm.showHelp(p);
		} else {
			if (Bukkit.getWorld(args[1]) != null) {
				p.setGameMode(GameMode.CREATIVE);
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mv modify set mode creative " + args[1]);
				p.sendMessage("§7 Teleporting to '" + args[1] + "'");
				mm.getServer().dispatchCommand(mm.getServer().getConsoleSender(), "mv tp " + p.getName() + " " + args[1]);
				mm.getServer().dispatchCommand(mm.getServer().getConsoleSender(), "mvconfirm");
				p.setGameMode(GameMode.CREATIVE);
				p.setFlying(true);
			} else {
				p.sendMessage(" §4Hmpf. There is no map called §6" + args[1] + "§4!");
				p.sendMessage(" §7(Type the ID, not the name!)");
			}
		}
	}

}
