package ch.levingo1.mapmanager.command;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;

import ch.levingo1.mapmanager.MapManager;
import ch.levingo1.mapmanager.util.Cuboid;

public class CmdCreate implements Cmd {
	private MapManager mm;

	public CmdCreate(MapManager mm, Player p, String[] args) {
		super();
		this.mm = mm;
		execute(p, args);
	}

	@Override
	public void execute(Player p, String[] args) {

		int id = mm.getConfig().getInt("maxId");
		int newid = id + 1;
		mm.getConfig().set("maxId", newid);
		p.sendMessage("§7§oCreating a new world with ID '" + id + "'");
		mm.getServer().dispatchCommand(mm.getServer().getConsoleSender(), "mv create " + id + " NORMAL -g VoidGenerator");

		

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

		mm.getConfig().set("worlds." + id + ".created", dateFormat.format(date));
		mm.getConfig().set("worlds." + id + ".owner", p.getName());
		mm.getConfig().set("worlds." + id + ".builders", new ArrayList<String>());
		mm.getConfig().set("worlds." + id + ".name", "Unnamed");

		ArrayList<String> worlds;

		try {
			worlds = (ArrayList<String>) mm.getConfig().getList("worldlist");
			worlds.add(String.valueOf(id));
		} catch (Exception ex) {
			worlds = new ArrayList<>();
			worlds.add(String.valueOf(id));
		}

		mm.getConfig().set("worldlist", worlds);

		mm.saveConfig();

		p.sendMessage("§7§oTip: If you broke the bedrock or just want to replace it with more blocks,");
		p.sendMessage("§7§oyou can generate a new spawn using /map buildspawn");
		p.sendMessage("§7§oRENAME THIS WORLD USING /map name <name>");
		
		World w = Bukkit.getWorld(String.valueOf(id));

		Bukkit.dispatchCommand(p, "map buildspawn " + id);

		p.teleport(w.getSpawnLocation());
		p.setGameMode(GameMode.CREATIVE);
		p.setFlying(true);

	}
}
