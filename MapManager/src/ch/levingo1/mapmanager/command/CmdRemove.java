package ch.levingo1.mapmanager.command;

import java.util.ArrayList;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import ch.levingo1.mapmanager.MapManager;

public class CmdRemove implements Cmd{
	private MapManager mm;

	public CmdRemove(MapManager mm, Player p, String[] args) {
		super();
		this.mm = mm;
		execute(p, args);
	}

	@Override
	public void execute(Player p, String[] args) {
		if (args.length != 1) {
			mm.showHelp(p);
		} else {
			if (mm.getConfig().getString("worlds." + p.getWorld().getName() + ".owner").equalsIgnoreCase(p.getName())) {
				String world = p.getWorld().getName();
				mm.getServer().dispatchCommand(mm.getServer().getConsoleSender(), "mv delete " + world);
				mm.getServer().dispatchCommand(mm.getServer().getConsoleSender(), "mv confirm");
				mm.getConfig().set("worlds." + world, null);

				ArrayList<String> worldsl;

				try {
					worldsl = (ArrayList<String>) mm.getConfig().getList("worldlist");
					worldsl.remove(world);
				} catch (Exception ex) {
					worldsl = new ArrayList<>();
				}

				mm.getConfig().set("worldlist", worldsl);
				mm.saveConfig();
				
				p.sendMessage("§4World '§6"+ world + "§4' has been removed!");

			} else {
				p.sendMessage("§4You're not the owner!");
			}
		}
	}
	
	
}
