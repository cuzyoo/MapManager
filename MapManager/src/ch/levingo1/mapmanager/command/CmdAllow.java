package ch.levingo1.mapmanager.command;

import java.util.ArrayList;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import ch.levingo1.mapmanager.MapManager;

public class CmdAllow implements Cmd{
	private MapManager mm;

	public CmdAllow(MapManager mm, Player p, String[] args) {
		super();
		this.mm = mm;
		execute(p, args);
	}

	@Override
	public void execute(Player p, String[] args) {
		if (args.length != 2) {
			mm.showHelp(p);
		} else {
			if (mm.getConfig().getString("worlds." + p.getWorld().getName() + ".owner").equalsIgnoreCase(p.getName())) {
				ArrayList<String> builders;

				try {
					builders = (ArrayList<String>) mm.getConfig().getList("worlds." + p.getWorld().getName() + ".builders");
					if (builders.contains(args[1])) {
						p.sendMessage("§6" + args[1] + " §4is already helping here...");
					} else {
						if (p.getName().equalsIgnoreCase(args[1])) {
							p.sendMessage("§4What do you want more?");
						} else {
							builders.add(args[1]);
							p.sendMessage("§aAdded §6" + args[1] + "§a to this map!");
						}
					}
				} catch (Exception ex) {
					builders = new ArrayList<>();
					builders.add(args[1]);
					p.sendMessage("§aAdded §6" + args[1] + "§a to this map!");
				}

				mm.getConfig().set("worlds." + p.getWorld().getName() + ".builders", builders);
				mm.saveConfig();

			} else {
				p.sendMessage("§4You're not the owner!");
			}

		}

	}
	
	
}
