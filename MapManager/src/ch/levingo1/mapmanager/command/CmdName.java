package ch.levingo1.mapmanager.command;

import java.util.ArrayList;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import ch.levingo1.mapmanager.MapManager;

public class CmdName implements Cmd{
	private MapManager mm;

	public CmdName(MapManager mm, Player p, String[] args) {
		super();
		this.mm = mm;
		execute(p, args);
	}

	@Override
	public void execute(Player p, String[] args) {
		if (args.length <= 1) {
			mm.showHelp(p);
		} else {
			if (mm.getConfig().getString("worlds." + p.getWorld().getName() + ".owner").equalsIgnoreCase(p.getName())) {
				
				StringBuilder sb = new StringBuilder();
				for (int i = 1; i < args.length; i++){
					sb.append(args[i]).append(" ");
				}
				 
				String name = sb.toString().trim();
				
				p.sendMessage("§aRenamed this world to §6" + name + "§a!");

				mm.getConfig().set("worlds." + p.getWorld().getName() + ".name", name);
				mm.saveConfig();

			} else {
				p.sendMessage("§4You're not the owner!");
			}

		}

	}
	
	
}
